package com.krist.smartdrive.gps;

import android.content.Context;

import java.util.ArrayList;

public class C1243a {
    private static C1243a f553h = new C1243a();
    public ArrayList f554a = new ArrayList();
    NMEAParser f555b;
    public ArrayList f556c = new ArrayList();
    public ArrayList f557d = new ArrayList();
    String f558e = "";

    private C1244b f562j = C1244b.FORMAT_DEG;
    private byte f563k;
    private int f564l;
    private int f565m;

    private C1243a() {
    }

    public static C1243a m1168a() {
        return f553h;
    }


    public void m1173a(C1244b c1244b) {
        this.f562j = c1244b;
    }

    public C1244b m1179b() {
        return this.f562j;
    }
    public void m1172a(Context context, String str) {
        if (this.f555b == null) {
            this.f555b = new NMEAParser(context, str);
        } else {
            this.f555b.m1361a(context, str);
        }
    }
    public ai m1180b(Context context, String str) {
        NMEAParser afVar = new NMEAParser(context, str);
        if (afVar.m1362a()) {
            return afVar.m1363b();
        }
        ai aiVar = new ai(context);
        aiVar.f763r = -1;
        return aiVar;
    }


    public int m1185d() {
        return this.f564l;
    }

    public void m1170a(int i) {
        this.f564l = i;
    }

    public int m1186e() {
        return this.f565m;
    }

    public void m1181b(int i) {
        this.f565m = i;
    }

    public void m1187f() {
        this.f554a.clear();
    }


}
