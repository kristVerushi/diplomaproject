package com.krist.smartdrive.gps;

public enum C1244b {
    FORMAT_DEG,
    FORMAT_MIN,
    FORMAT_SEC
}
