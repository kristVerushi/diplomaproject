package com.krist.smartdrive.gps;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;


public class NMEAParser {
    String f737a;
    final int f738b = 16;
    final int f739c = 13;
    final int f740d = 19;
    final int f741e = 10;
    final float f742f = 200.0f;
    Context f743g;

    public NMEAParser(Context context, String str) {
        this.f737a = str.trim();
        this.f743g = context;
    }

    public void m1361a(Context context, String str) {
        this.f737a = str.trim();
        this.f743g = context;
    }

    public boolean m1362a() {
        int i;
        int i2 = 0;
        for (i = 0; i < this.f737a.length(); i++) {
            char charAt = this.f737a.charAt(i);
            if (charAt != '$') {
                if (charAt == '*') {
                    break;
                }
                i2 ^= charAt;
            }
        }
        String[] split = this.f737a.split(",");
        if (split.length != 0) {
            String str = split[split.length - 1];
            String str2 = "";
            int lastIndexOf = str.lastIndexOf("*");
            if (lastIndexOf != -1) {
                str2 = str.substring(lastIndexOf + 1);
            }
            i = C1256n.m1243b(str2, -1520805211);
        } else {
            i = -1;
        }
        if (i2 == i) {
            return true;
        }
        return false;
    }

    public ai m1363b() {
        String str = "Decode";
        ai aiVar;
        if (m1362a()) {
            String[] split = this.f737a.split(",");
            if (split.length == 0) {
                aiVar = new ai(this.f743g);
                aiVar.f763r = -1;
                return aiVar;
            }
            String str2 = split[0];
            if (str2.length() != 6) {
                aiVar = new ai(this.f743g);
                aiVar.f763r = -1;
                return aiVar;
            }
            str2 = str2.substring(3, 6);
            if (str2.compareToIgnoreCase("GGA") == 0) {
                return m1352c();
            }
            if (str2.compareToIgnoreCase("RMC") == 0) {
                return m1354d();
            }
            if (str2.compareToIgnoreCase("GSA") == 0) {
                return m1356e();
            }
            if (str2.compareToIgnoreCase("VTG") == 0) {
                return m1358f();
            }
            if (str2.compareToIgnoreCase("GSV") == 0) {
                return m1360g();
            }
            Log.e(str, new StringBuilder(String.valueOf(str2)).append(" unknown !").toString());
            aiVar = new ai(this.f743g);
            aiVar.f763r = -1;
            aiVar.f764s = 32;
            return aiVar;
        }
        Log.e(str, "NMEA checksum error !");
        aiVar = new ai(this.f743g);
        aiVar.f763r = -1;
        return aiVar;
    }

    private ai m1348a(ai aiVar, String str) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -2;
        } else {
            int a = (int) C1256n.m1237a(str, -1.0f);
            if (((float) a) == -1.0f) {
                aiVar.f746a &= -2;
            } else {
                aiVar.f750e = a /*/ StatusCodes.AUTH_DISABLED*/;
                aiVar.f751f = (a /*(aiVar.f750e * StatusCodes.AUTH_DISABLED)*/) / 100;
                aiVar.f752g = (a/* - (aiVar.f750e * StatusCodes.AUTH_DISABLED)*/) - (aiVar.f751f * 100);
                aiVar.f746a |= 1;
            }
        }
        return aiVar;
    }

    private ai m1350b(ai aiVar, String str) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0 || str.length() != 6) {
            aiVar.f746a &= -65;
        } else {
            int a = C1256n.m1238a(str.substring(0, 2), -1);
            int a2 = C1256n.m1238a(str.substring(2, 4), -1);
            int a3 = C1256n.m1238a(str.substring(4, 6), -1);
            if (a3 == -1 || a2 == -1 || a == -1) {
                aiVar.f746a &= -65;
            } else {
                aiVar.f747b = a3 + 2000;
                aiVar.f748c = a2;
                aiVar.f749d = a;
                aiVar.f746a |= 64;
            }
        }
        return aiVar;
    }

    private ai m1349a(ai aiVar, String str, String str2) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -3;
        } else {
            float a = C1256n.m1237a(str, 200.0f);
            if (a == 200.0f) {
                aiVar.f746a &= -3;
            } else {
                float f = (float) ((int) (a / 100.0f));
                aiVar.f754i = (int) (((double) (((a - (100.0f * f)) / BitmapDescriptorFactory.HUE_YELLOW) + f)) * 1.0E7d);
                if (str2.compareToIgnoreCase("S") == 0) {
                    aiVar.f754i = -aiVar.f754i;
                }
                aiVar.f746a |= 2;
            }
        }
        return aiVar;
    }

    private ai m1351b(ai aiVar, String str, String str2) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -5;
        } else {
            float a = C1256n.m1237a(str, 200.0f);
            if (a == 200.0f) {
                aiVar.f746a &= -5;
            } else {
                float f = (float) ((int) (a / 100.0f));
                aiVar.f753h = (int) (((double) (((a - (100.0f * f)) / BitmapDescriptorFactory.HUE_YELLOW) + f)) * 1.0E7d);
                if (str2.compareToIgnoreCase("W") == 0) {
                    aiVar.f753h = -aiVar.f753h;
                }
                aiVar.f746a |= 4;
            }
        }
        return aiVar;
    }

    private ai m1353c(ai aiVar, String str) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -513;
        } else {
            float a = C1256n.m1237a(str, 200.0f);
            if (a == 200.0f) {
                aiVar.f746a &= -513;
            } else {
                aiVar.f760o = a;
                aiVar.f746a |= 512;
            }
        }
        return aiVar;
    }

    private ai m1355d(ai aiVar, String str) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -1025;
        } else {
            float a = C1256n.m1237a(str, 200.0f);
            if (a == 200.0f) {
                aiVar.f746a &= -1025;
            } else {
                aiVar.f761p = a;
                aiVar.f746a |= 1024;
            }
        }
        return aiVar;
    }

    private ai m1357e(ai aiVar, String str) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -129;
        } else {
            float a = C1256n.m1237a(str, -1.0f);
            if (a == -1.0f) {
                aiVar.f746a &= -129;
            } else {
                aiVar.f757l = a;
                aiVar.f746a |= 128;
            }
        }
        return aiVar;
    }

    private ai m1359f(ai aiVar, String str) {
        ai aiVar2 = new ai(this.f743g);
        if (str.length() == 0) {
            aiVar.f746a &= -257;
        } else {
            float a = C1256n.m1237a(str, -1.0f);
            if (a == -1.0f) {
                aiVar.f746a &= -257;
            } else {
                aiVar.f759n = a;
                aiVar.f746a |= 256;
            }
        }
        return aiVar;
    }

    private ai m1352c() {
        String str = "GGAHandler";
        ai aiVar = new ai(this.f743g);
        String[] a = C1256n.m1242a(this.f737a.replace('*', ','), ",");
        if (a.length < 16) {
            aiVar.f763r = -1;
            Log.e(str, "GGS para error !");
        } else {
            aiVar = m1351b(m1349a(m1348a(aiVar, a[1]), a[2], a[3]), a[4], a[5]);
            int a2 = C1256n.m1238a(a[6], -1);
            if (a2 != -1) {
                aiVar.f756k = a2;
                aiVar.f746a |= 8;
            } else {
                aiVar.f746a &= -9;
            }
            a2 = C1256n.m1238a(a[7], -1);
            if (a2 != -1) {
                aiVar.f758m = a2;
                aiVar.f746a |= 16;
            } else {
                aiVar.f746a &= -17;
            }
            aiVar = m1353c(aiVar, a[8]);
            float a3 = C1256n.m1237a(a[9], -999.99f);
            if (a3 != -999.99f) {
                aiVar.f755j = (int) a3;
                aiVar.f746a |= 32;
            } else {
                aiVar.f746a &= -33;
            }
            Log.i(str, "Flag=0x" + String.format("%X", new Object[]{Integer.valueOf(aiVar.f746a)}) + " " + this.f737a);
            aiVar.f763r = 0;
            aiVar.f764s = 1;
        }
        return aiVar;
    }

    private ai m1354d() {
        String str = "RMCHandler";
        ai aiVar = new ai(this.f743g);
        String[] a = C1256n.m1242a(this.f737a.replace('*', ','), ",");
        if (a.length < 13) {
            aiVar.f763r = -1;
            Log.e(str, "RMC para error !");
            return aiVar;
        }
        aiVar = m1350b(m1359f(m1357e(m1351b(m1349a(m1348a(aiVar, a[1]), a[3], a[4]), a[5], a[6]), a[7]), a[8]), a[9]);
        Log.i(str, "Flag=0x" + String.format("%X", new Object[]{Integer.valueOf(aiVar.f746a)}) + " " + this.f737a);
        aiVar.f763r = 0;
        aiVar.f764s = 2;
        return aiVar;
    }

    private ai m1356e() {
        String str = "GSAHandler";
        ai aiVar = new ai(this.f743g);
        String[] a = C1256n.m1242a(this.f737a.replace('*', ','), ",");
        if (a.length < 19) {
            aiVar.f763r = -1;
            Log.e(str, "GSA para error !");
            return aiVar;
        }
        int a2 = C1256n.m1238a(a[2], -1);
        if (a2 != -1) {
            aiVar.f762q = a2;
           /* aiVar.f746a |= Barcode.PDF417*/;
        } else {
            aiVar.f746a &= -2049;
        }
        aiVar = m1353c(m1355d(aiVar, a[15]), a[16]);
        Log.i(str, "Flag=0x" + String.format("%X", new Object[]{Integer.valueOf(aiVar.f746a)}) + " " + this.f737a);
        aiVar.f763r = 0;
        aiVar.f764s = 4;
        return aiVar;
    }

    private ai m1358f() {
        String str = "VTGHandler";
        ai aiVar = new ai(this.f743g);
        String[] a = C1256n.m1242a(this.f737a.replace('*', ','), ",");
        if (a.length < 10) {
            aiVar.f763r = -1;
            Log.e(str, "VTG para error !");
            return aiVar;
        }
        aiVar = m1357e(m1359f(aiVar, a[1]), a[5]);
        Log.i(str, "Flag=0x" + String.format("%X", new Object[]{Integer.valueOf(aiVar.f746a)}) + " " + this.f737a);
        aiVar.f763r = 0;
        aiVar.f764s = 8;
        return aiVar;
    }

    private ai m1360g() {
        ai aiVar = new ai(this.f743g);
        aiVar.f764s = 16;
        return aiVar;
    }
}
