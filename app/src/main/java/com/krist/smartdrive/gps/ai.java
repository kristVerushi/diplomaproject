package com.krist.smartdrive.gps;

import android.content.Context;



public class ai {
    int f746a;
    int f747b;
    int f748c;
    int f749d;
    int f750e;
    int f751f;
    int f752g;
    int f753h;
    int f754i;
    int f755j;
    int f756k;
    float f757l;
    int f758m;
    float f759n;
    float f760o;
    float f761p;
    int f762q;
    public int f763r;
    public int f764s;
    Context f765t;

    public ai(Context context) {
        this.f765t = context;
    }

    public String getLat() {
        int ordinal = C1243a.m1168a().m1179b().ordinal();
        if (ordinal == 0) {
            return String.format("%.6f", new Object[]{Double.valueOf(((double) this.f754i) / 1.0E7d)});
        } else if (ordinal == 1) {
            return m1367b(((double) this.f754i) / 1.0E7d, aj.LAT);
        } else {
            return m1366a(((double) this.f754i) / 1.0E7d, aj.LAT);
        }
    }
    public String getLng() {
        int ordinal = C1243a.m1168a().m1179b().ordinal();
        if (ordinal == 0) {
            return String.format("%.6f", new Object[]{Double.valueOf(((double) this.f753h) / 1.0E7d)});
        } else if (ordinal == 1) {
            return m1367b(((double) this.f753h) / 1.0E7d, aj.LNG);
        } else {
            return m1366a(((double) this.f753h) / 1.0E7d, aj.LNG);
        }
    }
    public String getSpeed(boolean z) {
        int i;
        String str = "";
        double[] dArr = new double[]{1.0d, 0.621371d};
        double d = ((double) this.f757l) * 1.852d;
        Object stringBuilder = new StringBuilder(String.valueOf("KM")).append("/").append("ore").toString();
        i = 0;
        if (z) {
            if (Double.valueOf(d * dArr[i])!=0) {


                return String.format("%.1f %s", new Object[]{Double.valueOf(d * dArr[i]), stringBuilder});
            }else {
                return null;
            }
        }
        return String.format("%.1f", new Object[]{Double.valueOf(dArr[i] * d)});
    }

    private String m1366a(double d, aj ajVar) {
        String str = "";
        double abs = Math.abs(d - ((double) ((int) d))) * 3600.0d;
        int abs2 = Math.abs((int) d);
        double d2 = abs - ((double) (((int) (abs / 60.0d)) * 60));
        str = "";
        str = ajVar == aj.LNG ? d > 0.0d ? "E" : "W" : d > 0.0d ? "N" : "S";
        return String.format("%d°%d'%.2f\"%s", new Object[]{Integer.valueOf(abs2), 2/*Integer.valueOf(abs)*/, Double.valueOf(d2), str});
    }

    private String m1367b(double d, aj ajVar) {
        String str = "";
        double abs = Math.abs(d - ((double) ((int) d)));
        int abs2 = Math.abs((int) d);
        double d2 = 60.0d * abs;
        str = "";
        str = ajVar == aj.LNG ? d > 0.0d ? "E" : "W" : d > 0.0d ? "N" : "S";
        return String.format("%d°%.3f'%s", new Object[]{Integer.valueOf(abs2), Double.valueOf(d2), str});
    }
}
