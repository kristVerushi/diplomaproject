package com.krist.smartdrive.gps;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class C1256n {
    public static boolean m1241a(Context context) {
        //NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return true;
    }

    public static boolean m1245b(Context context) {
        boolean a = C1256n.m1241a(context);
        boolean b =/* C1255m.m1232b(context)*/false;
        int a2 = /*C1255m.m1227a(context)*/1;
        Log.v("Util:", "bRate=" + b + " launchCnt=" + a2);
        if (b) {
            return false;
        }
        if (a2 % 3 != 0) {
            //C1255m.m1228a(context, a2 + 1);
            return false;
        } else if (a) {
            return true;
        } else {
            return false;
        }
    }

 /*   public static void m1246c(Context context) {
        int a = C1255m.m1227a(context) + 1;
        C1255m.m1228a(context, a);
        Log.v("Util:", "rate dialog cancel , launchCnt=" + a);
    }*/

    public static String m1239a(String str) {
        String str2 = "";
        int lastIndexOf = str.lastIndexOf("/");
        if (lastIndexOf != -1) {
            return str.substring(lastIndexOf + 1, str.length());
        }
        return str2;
    }

    public static String m1244b(String str) {
        String str2 = "";
        int lastIndexOf = str.lastIndexOf("/");
        if (lastIndexOf != -1) {
            return str.substring(0, lastIndexOf);
        }
        return str2;
    }

    public static int m1238a(String str, int i) {
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
        }
        return i;
    }

    public static float m1237a(String str, float f) {
        try {
            f = Float.parseFloat(str);
        } catch (NumberFormatException e) {
        }
        return f;
    }

    public static int m1243b(String str, int i) {
        try {
            i = Integer.parseInt(str, 16);
        } catch (NumberFormatException e) {
        }
        return i;
    }

    public static String[] m1242a(String str, String str2) {
        List arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(str2, i);
            if (indexOf == -1) {
                arrayList.add(str.substring(i, str.length()));
                return (String[]) arrayList.toArray(new String[arrayList.size()]);
            }
            arrayList.add(str.substring(i, indexOf));
            i = indexOf + 1;
        }
    }

    public static void m1240a(Context context, String[] strArr, String str, String str2) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("mailto:"));
        intent.putExtra("android.intent.extra.EMAIL", strArr);
        intent.putExtra("android.intent.extra.SUBJECT", str);
        intent.putExtra("android.intent.extra.TEXT", str2);
        try {
            context.startActivity(Intent.createChooser(intent, "Choose an email client from..."));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No email client installed.", Toast.LENGTH_LONG).show();
        }
    }
/*
    public static String m1247d(Context context) {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        return context.getString(R.string.app_name) + " (" + str + "_" + str2 + ", V" + VERSION.RELEASE + ")";
    }*/
}
