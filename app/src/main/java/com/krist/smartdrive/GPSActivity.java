package com.krist.smartdrive;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.krist.smartdrive.gps.C1243a;
import com.krist.smartdrive.gps.ai;

import java.io.InputStream;

public class GPSActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView latitude;
    private TextView longnitude;
    private TextView speed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        latitude = ((TextView) findViewById(R.id.latitude));

        longnitude = ((TextView) findViewById(R.id.longnitude));
        speed = ((TextView) findViewById(R.id.speed));
    }

    String[] lines;

    private void readNmea() {
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.simulatednmea);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            String fileString = new String(b);
            // Log.e("NMEA", fileString);
            lines = fileString.split(System.getProperty("line.separator"));

            startTime();
           /* for (String line : lines) {
                Log.e("NMEA", line);
            }*/

        } catch (Exception e) {
            // e.printStackTrace();
            Log.e("NMEA", "ERRoR");
        }

    }

    void startTime() {
        //Post handler after 1 second.
        timer.postDelayed(runnable, 100);
    }

    int positionIndex=0;
    Handler timer = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {


            if (lines!=null && positionIndex<lines.length){
                GPSActivity.this.m1319b(lines[positionIndex]);
                Log.d("PARSED",lines[positionIndex]);
                positionIndex++;
            }

            timer.postDelayed(runnable, 100);

        }
    };

    public void m1319b(String str) {
        ai b = C1243a.m1168a().m1180b(this, str);
        if (b.f763r == 0) {
          //  m1312a(b);
        }
        String lat = b.getLat();
        String lng = b.getLng();
        String speed = b.getSpeed(true);

        Log.e("LATLNG", lat +" ,"+ lng +"    ");

        double latitude = Double.parseDouble(lat);
        double longnitude = Double.parseDouble(lng);

        if (latitude !=0){

            LatLng latLng = new LatLng(latitude, longnitude);
            this.latitude.setText(lat);
            this.longnitude.setText(lng);
            if (speed!=null) {
                this.speed.setText(speed);

            }
            markmap(latLng);
           // mMap.
            //writeALineInMap();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.removeCallbacks(runnable);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



        readNmea();
    }

    LatLng lastPoint=null;

    public void markmap( LatLng newPoint){
       // LatLng initPoint = new LatLng(-34, 151);
        /*mMap.addMarker(new MarkerOptions().position(initPoint).title("Start"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(initPoint));*/
        if (lastPoint!=null) {
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .add(lastPoint, newPoint)
                    .width(20)
                    .color(Color.RED));

        }
        lastPoint=newPoint;
    }
}
